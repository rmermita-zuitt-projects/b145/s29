let http = require('http');

let directory = [
{
	"name" : "Tony Stark",
	"email" : "stark3000@mail.com"
},
{
	"name" : "Peter Parker",
	"email" : "nowayhome@mail.com"
}
]

http.createServer((request, response) => {

	if (request.url == "/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to my website!');
	};

	//Mini-Activity
	//Create an endpoint "/items" with a request method of POST
	//Make the status code 200 and content type of text.
	//Responsoe should say 'Data to be sent to the database.'

	if (request.url == "/items" && request.method == "POST") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Data to be sent to the database.');
	};

	if (request.url == '/users' && request.method == 'GET') {
	response.writeHead(200, {'Content-Type' : 'application/json'})
	response.write(JSON.stringify(directory));
	response.end();
	
	};

	if (request.url == "/users" && request.method == "POST") {
		let requestBody = '';
		request.on ('data', (data) => {
			requestBody += data;
		});

		request.on('end', () => {
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				'name' : requestBody.name,
				'email' : requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	};

}).listen(4000);

console.log('Server is running at port 4000');