let http = require('http');

http.createServer((request, response) => {

	if (request.url == "/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to booking system!');
	};

	if (request.url == "/profile" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to your profile!');
	};

	if (request.url == "/courses" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Here's our available courses!");
	};

	if (request.url == "/addCourse" && request.method == "POST") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Add a course to our resources!");
	};

	if (request.url == "/updatecourse" && request.method == "PUT") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Update a course to our resources!");
	};

	if (request.url == "/archiveCourse" && request.method == "DELETE") {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Archive courses to our resources!");
	};


console.log('Starting');
}).listen(4000);